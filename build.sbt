val scala3Version = "3.0.2"

ThisBuild / scalaVersion := scala3Version

lazy val root = project
  .in(file("."))
  .settings(
    name := "dining-philosophers",
    version := "0.1.0",
    scalaVersion := scala3Version,
    libraryDependencies ++=
      Seq(
        "org.typelevel" %% "cats-effect" % "2.5.1",
        "dev.zio" %% "zio" % "2.0.0-M2",
        "tf.tofu" %% "tofu-zio-logging" % "0.10.6",
      ).map(_.cross(CrossVersion.for3Use2_13)),
      libraryDependencies += 
        "org.scalatest" %% "scalatest" % "3.2.10" % Test
  )