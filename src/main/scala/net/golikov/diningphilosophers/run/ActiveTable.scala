package net.golikov.diningphilosophers.run

import net.golikov.diningphilosophers.EnumeratedFork
import net.golikov.diningphilosophers.EnumeratedPhilosopher
import net.golikov.diningphilosophers.EnumeratingTable
import net.golikov.diningphilosophers.Philosopher
import zio._
import scala.util.Try

class ActiveTable(val table: EnumeratingTable):
  val allForks = Chunk.fromIterable(table.allForks).mapZIO(ForkInUse(_))
  val allPhilosophers = allForks.flatMap(allForks =>
    Chunk
      .fromIterable(table.allPhilosophers)
      .foldZIO((Seq.empty[ActivePhilosopher], allForks)) { case ((s, allForks), p) =>
        val (rightFork, leftFork) = table.forks(p)
        for {
          status <- Ref.make(Philosopher.Status.Thinking)
        } yield (s :+ ActivePhilosopher(p, allForks(rightFork.number), allForks(leftFork.number), status), allForks)
      }
  )

object ActiveTable extends zio.App:
  def run(args: List[String]) =
    ZIO
      .fromTry(Try(args(0).toInt))
      .catchAll(_ => ZIO.succeed(5))
      .flatMap(start)
      .exitCode

  private def start(philosophersCount: Int) =
    val table = new ActiveTable(EnumeratingTable(philosophersCount))
    for {
      fibers <- table.allPhilosophers.map(_._1)
      startedFibers <- Chunk.fromIterable(fibers).mapZIO(_.start())
      _ <- Fiber.joinAll(startedFibers)
    } yield ()
