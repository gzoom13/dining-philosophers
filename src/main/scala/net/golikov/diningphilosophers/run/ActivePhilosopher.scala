package net.golikov.diningphilosophers.run

import net.golikov.diningphilosophers.EnumeratedFork
import net.golikov.diningphilosophers.EnumeratedPhilosopher
import net.golikov.diningphilosophers.Philosopher
import zio._

import java.io.IOException

class ActivePhilosopher(
    val enumerated: EnumeratedPhilosopher,
    val rightFork: ForkInUse,
    val leftFork: ForkInUse,
    private val status: Ref[Philosopher.Status]
):
  private val number = enumerated.number
  private def orderedForks = Chunk(rightFork, leftFork).sortBy(_.number)

  def start(): ZIO[Has[Console] with Has[Random] with Has[Clock], Nothing, Fiber[IOException, Unit]] =
    (for {
      previousStatus <- status.get
      newStatus <- maybeSwitch(previousStatus)
      _ <- newStatus.fold(
        ZIO.unit
      )(s =>
        Console.printLine(s"Philosopher $number will try to be $s now") *>
          (s match {
            case Philosopher.Status.Thinking => think()
            case Philosopher.Status.Eating   => eat()
            case Philosopher.Status.Waiting  => ZIO.unit
          })
      )
      timeToNextPossibleSwitch <- Random.nextInt.map(_ % 5 + 5).map(_.seconds)
      _ <- ZIO.sleep(timeToNextPossibleSwitch)
    } yield ()).forever.fork

  private def eat(): ZIO[Has[Console], IOException, Unit] =
    for {
      _ <- setStatus(Philosopher.Status.Waiting)
      _ <- orderedForks.mapZIO(fork =>
        fork.used.acquire.commit <* Console.printLine(s"Philosopher $number took fork ${fork.number}")
      )
      _ <- setStatus(Philosopher.Status.Eating)
    } yield ()

  private def think(): ZIO[Has[Console], IOException, Unit] =
    for {
      _ <- orderedForks.reverse.mapZIO(fork =>
        fork.used.release.commit <* Console.printLine(s"Philosopher $number put fork ${fork.number} down")
      )
      _ <- setStatus(Philosopher.Status.Thinking)
    } yield ()

  private def maybeSwitch(
      previousStatus: Philosopher.Status
  ): ZIO[Has[Random], IOException, Option[Philosopher.Status]] =
    val potentialNewStatus = ZIO.succeed(previousStatus match
      case Philosopher.Status.Thinking => Some(Philosopher.Status.Eating)
      case Philosopher.Status.Eating   => Some(Philosopher.Status.Thinking)
      case Philosopher.Status.Waiting  => None
    )
    potentialNewStatus.flatMap {
      case None    => ZIO.none
      case Some(_) => ZIO.ifZIO(Random.nextBoolean)(potentialNewStatus, ZIO.none)
    }

  private def setStatus(newStatus: Philosopher.Status): ZIO[Has[Console], IOException, Unit] =
    for {
      _ <- status.set(newStatus)
      _ <- Console.printLine(s"Philosopher $number is now $newStatus")
    } yield ()
