package net.golikov.diningphilosophers.run

import zio.Semaphore
import net.golikov.diningphilosophers.EnumeratedFork

class ForkInUse(val used: Semaphore, private val enumerated: EnumeratedFork):
  val number: Int = enumerated.number

object ForkInUse:
  def apply(enumerated: EnumeratedFork) = Semaphore.make(1).map(new ForkInUse(_, enumerated))
