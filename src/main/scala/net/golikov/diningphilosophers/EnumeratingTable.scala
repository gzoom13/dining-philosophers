package net.golikov.diningphilosophers

trait Enumerated:
  def number: Int

case class EnumeratedPhilosopher(override val number: Int) extends Philosopher with Enumerated
case class EnumeratedFork(override val number: Int) extends Fork with Enumerated

class EnumeratingTable(val size: Int) extends Table[EnumeratedPhilosopher, EnumeratedFork]:
  val allPhilosophers = (0 until size).map(EnumeratedPhilosopher(_)).toVector
  val allForks = (0 until size).map(EnumeratedFork(_)).toVector

  override def forks(philosopher: EnumeratedPhilosopher): (EnumeratedFork, EnumeratedFork) =
    (allForks(philosopher.number), allForks((philosopher.number + 1) % size))

  override def philosophers(fork: EnumeratedFork): (EnumeratedPhilosopher, EnumeratedPhilosopher) =
    (allPhilosophers(fork.number), allPhilosophers((fork.number + 1) % size))

