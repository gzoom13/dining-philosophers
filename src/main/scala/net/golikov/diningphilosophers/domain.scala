package net.golikov.diningphilosophers

import zio._

import java.io.IOException
import scala.util.Try

trait Philosopher

object Philosopher:
  enum Status:
    case Thinking, Eating, Waiting

trait Fork

trait Table[P <: Philosopher, F <: Fork]:
  def forks(p: P): (F, F)
  def philosophers(f: F): (P, P)


