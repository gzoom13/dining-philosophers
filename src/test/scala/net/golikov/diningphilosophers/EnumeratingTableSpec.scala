package net.golikov.diningphilosophers

import collection.mutable.Stack
import org.scalatest._
import flatspec._
import matchers._
import scala.language.postfixOps

class EnumeratingTableSpec extends AnyFlatSpec with should.Matchers {

  "Only two philosophers behind the same table" should "have both forks common" in new TestTable(2) {
    table.forks(EnumeratedPhilosopher(0)).swap shouldEqual table.forks(EnumeratedPhilosopher(1))
  }

  "Ony two forks on the same table" should "have both philosophers common" in new TestTable(2) {
    table.philosophers(EnumeratedFork(0)).swap shouldEqual table.philosophers(EnumeratedFork(1))
  }

  "Every philosopher behind the table for three" should "have common fork wiht another one" in new TestTable(3) {
    (0 until 3).map(i => table.forks(EnumeratedPhilosopher(i))) shouldEqual
      Seq((0, 1), (1, 2), (2, 0)).map(t => (EnumeratedFork(t._1), EnumeratedFork(t._2)))
  }

  "Every fork on the table for three" should "have common philosopher wiht another one" in new TestTable(3) {
    (0 until 3).map(i => table.philosophers(EnumeratedFork(i))) shouldEqual
      Seq((0, 1), (1, 2), (2, 0)).map(t => (EnumeratedPhilosopher(t._1), EnumeratedPhilosopher(t._2)))
  }

  trait TestTable(size: Int):
    val table = EnumeratingTable(size)

}
